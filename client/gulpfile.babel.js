var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync').create(),
    concat = require('gulp-concat'),
    uglifyjs = require('gulp-uglifyjs'),
    cssnano = require('gulp-cssnano'),
    rename = require('gulp-rename'),
    autoprefixer = require('gulp-autoprefixer'),
    del = require('del'),
    sourcemaps = require('gulp-sourcemaps');

var jsLibs = ['./src/libs/'];

const settings = {
    publicFolder: './public',
};

function reload(done) {
    browserSync.reload();
    done();
}

gulp.task('sass', function() {
    return gulp
        .src('./src/scss/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({ outputStyle: 'expanded' }))
        .pipe(
            autoprefixer({
                browsers: ['last 50 versions'],
            })
        )
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(settings.publicFolder + '/css'));
});

gulp.task(
    'init',
    gulp.series('sass', function() {
        browserSync.init({
            server: {
                baseDir: settings.publicFolder,
            },
            notify: false,
        });
        gulp.watch('./src/scss/**/*.scss', gulp.series('sass', reload));
        gulp.watch(settings.publicFolder + '/**/*.html').on(
            'change',
            browserSync.reload
        );
        gulp.watch(settings.publicFolder + '/**/*.js').on(
            'change',
            browserSync.reload
        );
    })
);

gulp.task(
    'watch',
    gulp.series('sass', () => {
        gulp.watch('./src/scss/**/*.scss', gulp.series('sass'));
    })
);

gulp.task('default', gulp.series('init'));

gulp.task('css-libs', function(done) {
    return gulp
        .src('src/css/libs.css')
        .pipe(cssnano())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(settings.publicFolder + '/css'));
});

gulp.task('js-libs', function(done) {
    return gulp
        .src(jsLibs)
        .pipe(concat('libs.min.js'))
        .pipe(uglifyjs())
        .pipe(gulp.dest('src/js'));
});

gulp.task('libs', gulp.series('sass', 'css-libs', 'js-libs'));
